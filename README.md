Upload your files and folders to Dracoon via the desktop client and store them in a cloud. Generating a link for sharing with open or restricted access is possible. Pausing downloads and removing files is possible. Dracoon Desktop is a neat cloud-based application that serves as a lesser-known, but competent alternative to services such as Dropbox of Google Drive. In its core Dracoon Desktop is a client for the Web service of the same name. It provides a simple interface that helps users synchronize files between several devices, both stationary and mobile, and access such files at any moment. All types of data that you have stored using Dracoon are available at all times from any location, they can be shared with other users, and you also make sure that your information is not lost if the physical data storage device is broken or destroyed.

There are also some additional options concerning shared file access and document collaboration, as well as advanced file/folder sharing options

Overall, Dracoon Desktop is a competent app for accessing and using a solid developing cloud storage service. It has several useful features worth the attention.

* copy files folders
* move files folders
* rename files folders
* delete files folders
* upload download files folders
* share files folders
* list shared files folders
* list bookmarked files folders
* manage data rooms (create, edit, delete)
* Send files from dracoon account to [Supported Cloud Services](https://github.com/loudKode/Easy-Cloud-Integration/wiki/Supported-Cloud-Services) with ability to specify transferred [video resolution](https://i.postimg.cc/HsVnV1tC/ure-Wiz660.png)


![https://i.postimg.cc/YCnSJBtc/Dracoon-Client-Scef-Vw-Qk-Gx.png](https://i.postimg.cc/YCnSJBtc/Dracoon-Client-Scef-Vw-Qk-Gx.png)

![https://i.postimg.cc/HxdQCZ2Z/ure-Wiz122.png](https://i.postimg.cc/HxdQCZ2Z/ure-Wiz122.png)

![https://i.postimg.cc/xdyzgG2K/ure-Wiz123.png](https://i.postimg.cc/xdyzgG2K/ure-Wiz123.png)

![https://i.postimg.cc/DyHpKhbx/ure-Wiz124.png](https://i.postimg.cc/DyHpKhbx/ure-Wiz124.png)

![https://i.postimg.cc/BQrpVCdy/ure-Wiz125.png](https://i.postimg.cc/BQrpVCdy/ure-Wiz125.png)

![https://i.postimg.cc/cLG7mL3G/ure-Wiz126.png](https://i.postimg.cc/cLG7mL3G/ure-Wiz126.png)

![https://i.postimg.cc/nLz1sw7X/ure-Wiz127.png](https://i.postimg.cc/nLz1sw7X/ure-Wiz127.png)

![https://i.postimg.cc/yY13Y30d/ure-Wiz128.png](https://i.postimg.cc/yY13Y30d/ure-Wiz128.png)

![https://i.postimg.cc/Kvx3M9PV/ure-Wiz422.png](https://i.postimg.cc/Kvx3M9PV/ure-Wiz422.png)

.

* Download:
[https://github.com/jamesheck2019/Unofficial-Dracoon-Client/releases](https://github.com/jamesheck2019/Unofficial-Dracoon-Client/releases)